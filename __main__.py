#!/usr/local/lib/anaconda/envs/user-management/bin/python

""" g2g

This python program aides in emulating "nested grouping" in linux machines by adding users from one group to another.

Usage: g2g [-h, --help] dest_group copied_group

dest_group: The destination group. It is the group that will have users added *to* it. It must exist.
copied_group: The copied group. It is the group that will have users copied *from* it. It must exist.
"""

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

from argparse import ArgumentParser, Action, ArgumentError
from datetime import datetime, date
import subprocess, sys, linecache, logging, logging.handlers, os

# ---------------------------------------------------------------------------------------------------------------------------------------
# Import file path libraries depending on the operating system; only linux is supported
# ---------------------------------------------------------------------------------------------------------------------------------------
osData = sys.platform.lower()
if osData.startswith('linux'):
    import grp
else:
    raise RuntimeError('The current operating system is not supported.')

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

this = sys.modules[__name__]
this.g_isVerbose = False
this.g_logFile = ''
this.g_logToFile = False
this.g_logger = None

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

# ---------------------------------------------------------------------------------------------------------------------------------------
# LOGGING
# ---------------------------------------------------------------------------------------------------------------------------------------

class LoggingConsole(object):

    @staticmethod
    def LogError(msg):
        logMsg = "[{datetime}][ERROR]: {message}".format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')),message=msg)
        if not this.g_logToFile:
            print(logMsg)
        else:
            this.g_logger.error(logMsg)

    @staticmethod
    def LogSuccess(msg):
        logMsg = "[{datetime}][SUCCESS]: {message}".format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')), message=msg)
        if not this.g_logToFile and this.g_isVerbose:
            print(logMsg)
        else:
            this.g_logger.info(logMsg)

    @staticmethod
    def LogWarning(msg):
        logMsg = "[{datetime}][WARNING]: {message}".format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')), message=msg)
        if not this.g_logToFile and this.g_isVerbose:
            print(logMsg)
        else:
            this.g_logger.warning(logMsg)

    @staticmethod
    def LogVerbose(msg):
        logMsg = "[{datetime}][VERBOSE]: {message}".format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')), message=msg)
        if not this.g_logToFile and this.g_isVerbose:
            print(logMsg)
        else:
            this.g_logger.info(logMsg)

    @staticmethod
    def GetFormattedException():
        exc_type, exc_obj, tb = sys.exc_info()
        f = tb.tb_frame
        lineno = tb.tb_lineno
        filename = f.f_code.co_filename
        linecache.checkcache(filename)
        line = linecache.getline(filename, lineno, f.f_globals)
        return 'EXCEPTION IN ({}, LINE {} "{}"): {} \r'.format(filename, lineno, line.strip(), exc_obj)

    @staticmethod
    def CreateRotatingLog():
        # Create a logger with a log level set by the user
        this.g_logger = logging.getLogger("Cleanusr Log")
        logLevel = logging.DEBUG if this.g_isVerbose else logging.ERROR
        this.g_logger.setLevel(logLevel)

        # Create a timed rotating file handler that will rotate the logs every Monday (W0).
        # It will only keep 8 backup logs, or 8 weeks of logs.
        handler = logging.handlers.TimedRotatingFileHandler(this.g_logFile, when="W0", backupCount=8)
        this.g_logger.addHandler(handler)

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

# ---------------------------------------------------------------------------------------------------------------------------------------
# ARGUMENT PARSING
# ---------------------------------------------------------------------------------------------------------------------------------------

class GroupExistsAction(Action):
    """ Verifies that the group exists and adds the group object itself as the attibute
    """
    def __call__(self, parser, namespace, values, option_string=None):
        # Verify that the linux group exists
        try:
            group = grp.getgrnam(values)
        except KeyError:
            message = '[ERROR]: The group, {group}, does not exist.'.format(group=values)
            raise ArgumentError(self, message)

        # Add the attribute
        setattr(namespace, self.dest, group)

def get_script_path():
    return os.path.dirname(os.path.realpath(sys.argv[0]))

def collect_args():
    """ Uses an argument parser to gather and parse arguments given by the user in the command line.

    Returns:
        dict: A dictionary containing the keys "dest_group" and "copied_group"
    """

    # Default log file location
    logDir = os.path.join(get_script_path(), 'log')

    # Setup arguments to parse
    argParser = ArgumentParser(description='Emulates nested grouping for linux machines by adding users from one group to another.')

    # Logging arguments
    argParser.add_argument('-l', '--log', action='store_true', help='Whether or not to log to a file. The logs will rotate every Monday and only 8 log files will be kept at a time.')
    argParser.add_argument('-ld', '--logdir', default=logDir, help='If logging to a file, the log file directory to save the log to. By default, it is %(default)s.')
    argParser.add_argument('-v', '--verbose', action='store_true', help='Whether or not to log verbose messages and datetime stamps. Errors will still be logged.')

    # Positional arguments
    argParser.add_argument(
        'dest_group',
        action = GroupExistsAction,
        help = 'The destination group. It is the group that will have users added *to* it.'
    )
    argParser.add_argument(
        'copied_group',
        action = GroupExistsAction,
        help = 'The copied group. It is the group that will have users copied *from* it.'
    )

    # Parse arguments and return them
    args = vars(argParser.parse_args())
    this.g_isVerbose = args['verbose']
    this.g_logDir = args['logdir']
    this.g_logToFile = args['log']

    # If we're logging, make sure to create all directories needed in the path if they don't exist
    try:
        if this.g_logToFile:
            if not os.path.isdir(this.g_logDir):
                os.makedirs(this.g_logDir)
            this.g_logFile = os.path.join(this.g_logDir, 'g2glog')
    # Don't continue if the log dir couldn't be created
    except Exception as ex:
        if not os.path.isdir(this.g_logDir):
            raise
        else:
            LoggingConsole.LogError("Problem creating the parent log directory. {msg}".format(msg=LoggingConsole.GetFormattedException()))

    return args

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

# ---------------------------------------------------------------------------------------------------------------------------------------
# ADDING A GROUP TO ANOTHER GROUP
# ---------------------------------------------------------------------------------------------------------------------------------------

def add_group_to_group(dest_group, copied_group):

    # Ensure that we only add users from the copied group that are not members of the destination group
    group_member_diff = [member for member in copied_group.gr_mem if member not in dest_group.gr_mem]

    # Print the users that will be added
    LoggingConsole.LogVerbose('=========================================================================================')
    LoggingConsole.LogVerbose('ADDING USERS')
    LoggingConsole.LogVerbose('=========================================================================================')
    if len(group_member_diff) > 0:
        LoggingConsole.LogVerbose('The following users will be added to {dest} from {copied}'.format(dest=dest_group.gr_name, copied=copied_group.gr_name))
        LoggingConsole.LogVerbose(','.join(group_member_diff))
    else:
        LoggingConsole.LogVerbose('No users will be added to {dest} from {copied}'.format(dest=dest_group.gr_name, copied=copied_group.gr_name))
    print('')


    # Add users to destination group
    try:
        for username in group_member_diff:
            subprocess.check_call("usermod -a -G {dest} {user}".format(dest=dest_group.gr_name, user=username), shell=True)
    except subprocess.CalledProcessError as ex:
        LoggingConsole.LogError(ex.output)

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

# ---------------------------------------------------------------------------------------------------------------------------------------
# MAIN METHOD
# ---------------------------------------------------------------------------------------------------------------------------------------

def main():
    """This program will allow you to emulate "nested groups" in linux by adding users from one group to another.  
    """

    # Parse arguments
    args = collect_args()

    # Create a rotating log (if specified by user)
    if this.g_logToFile:
        LoggingConsole.CreateRotatingLog()

    # Add users from copied group to the destination group
    add_group_to_group(args['dest_group'], args['copied_group'])

    # Let the user know that the script has finished
    LoggingConsole.LogVerbose('=========================================================================================')
    LoggingConsole.LogVerbose('FINISHED')
    LoggingConsole.LogVerbose('=========================================================================================')
    LoggingConsole.LogVerbose('')

if __name__ == "__main__":
    main()

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
