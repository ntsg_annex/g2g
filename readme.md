# g2g

## Dependencies

- Linux Only
- Python >= 2.7


## Overview

This python program aides in emulating "nested grouping" in linux machines by adding users from one group to another.

## Usage

```
#!bash

python g2g [-h, --help] [-v, --verbose] [-l, --log] [-ld LOGDIR] dest_group copied_group
```

| Argument          | Description                                                                                  | Required? | Default    |
| ----------------- | -------------------------------------------------------------------------------------------- | --------- | ---------- |
| `-h`, `--help`    | Show help message and exit                                                                   | No        | N/A        |
| `dest_group`      | The destination group. It is the group that will have users added *to* it. It must exist.    | Yes       | N/A        |
| `copied_group`    | The copied group. It is the group that will have users copied *from* it. It must exist.      | Yes       | N/A        |
| `-l, --log`       | Whether or not to log the output.                                                            | No        | N/A        |
| `-ld, --logdir`   | The log file directory.                                                                      | No        | (see below)|
| `-v, --verbose`   | Whether or not to log verbose messages and datetime stamps. Errors will still be logged.     | No        | N/A        |

*DEFAULT FOR LOGDIR*: `path-to-g2g/log`. This program will rotate the logs in this directory every Monday and will only keep 8 logs at a time. Or, in other words, it will only keep 8 weeks of logs around at a time.

In order to get an up-to-date version of the arguments that are passed in to the module, all you have to do is type:
```
#!bash

python g2g --help
```
And you should get a listing of the different arguments that can be passed in.


-------------------------------------------------------------------------------

** Script Author:** Angela Gross